package by.general.SuleimanovSpringCrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuleimanovSpringCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuleimanovSpringCrudApplication.class, args);
	}

}
